# Applique des règles sur des lexiques ou des corpus

Applique des règles listées dans un fichier à part à un flux de données type lexique ou corpus.

Ce script peut fonctionner en deux modes:
  * Mode tabulaire (par défaut): le script traite des données tabulaires.
  * Mode mixte (option `-x`): le script traite un mélange de XML et de données tabulaires.

L'écriture de règles nécessite des compétences de base en CQP, en expressions régulières, et, pour une utilisation avancée, des bases de _XML_ et de _Perl_.

## Algorithme
Dans le cas de règles simples, le script traite les données en entrée ligne par ligne, et essaye d'appliquer successivement chaque règle sur chacune de ces lignes.

Dans le cas de règles comportant plusieurs lignes en entrée (du type "si token_1 suivi de token_2, faire qqch"), le script crée une fenêtre de _n_ tokens (_n_ est déterminé automatiquement pour prendre en compte toutes les règles) qu'il fait défiler sur toutes les lignes.

Les règles sont de type "si un token a un pattern donné, alors écrire à la place un token qui a telles propriétés".

### Mode tabulaire
Le script attend des données tabulaires, utilisant la tabulation comme séparateur. Par défaut, la première ligne est considérée comme un _header_ (nom des colonnes); si ce n'est pas le cas, on peut utiliser l'option `-h` pour passer un _header_ (par exemple `-h "form,pos,lemma"`).

Exemple de données tabulaires:
```csv
form  lemma         pos
1     _MOT_NOMBRE   Mc
2     _MOT_NOMBRE   Mc
3     _MOT_NOMBRE   Mc
lorem LOREM         Xe
ipsum IPSUM         Xe
test  TEST          Nc
test  AUTRE         Nc
```

### Mode mixte
Le script attend des données formatées de façon particulière: chaque ligne du texte normalisé contient soit du texte formaté de manière tabulaire (comme en mode tabulaire), soit *une* balise. Cela correspond à la sortie du script prestoNormaliser. Si une ligne contient du texte *et* des balises, et qu'une règle est appliquée sur cette ligne, les balises de la ligne seront effacées. Sinon, elles sont conservées.

Exemple de données mixtes:
```xml
form  pos
<presto:pretag pos="Xe">
Lorem Xe
Ipsum Xe
</presto:pretag>
```

## Syntaxe des règles
La syntaxe des règles s'inspire de CQP.

### Règle de suppression simple
La règle `[lemma="_MOT_NOMBRE"]` supprime tous les tokens dont la propriété (=colonne) `lemma` a pour valeur `_MOT_NOMBRE`.
On peut utiliser plusieurs propriétés par token. La règle `[lemma="TEST" form="test"]` supprime les tokens dont la propriété `lemma` vaut `TEST` *et* dont la propriété `form` vaut `test`. Ainsi, avec les deux règles précédentes, le lexique suivant:
```csv
form  lemma         pos
1     _MOT_NOMBRE   Mc
2     _MOT_NOMBRE   Mc
3     _MOT_NOMBRE   Mc
lorem LOREM         Xe
ipsum IPSUM         Xe
test  TEST          Nc
test  AUTRE         Nc
```
sera réécrit:
```csv
form  lemma         pos
lorem LOREM         Xe
ipsum IPSUM         Xe
test  AUTRE         Nc
```

Outre l'opérateur `=`, les opérateurs `!=`, `<`, `<=`, `>=` et `>` sont disponibles. Par exemple, la règle `[form!=/.*y.*/ lemma="ANNALISTE"]` supprime tous les tokens dont la forme contient un `y` et dont le lemme est `ANNALISTE`. Voir plus loin _Utilisation de propriétés tirées des balises_ pour un exemple avec l'opérateur `<`.

### Règle de suppression par regex
La règle `[lemma=/[0-9]/]` supprime tous les tokens dont la propriété (=colonne) `lemma` *est exactement* un chiffre. La règle `[form=/.*[0-9].*/]` supprime tous les tokens dont la propriété `form` *contient* un chiffre.

### Règle de substitution simple
La règle `[lemma="SOEUR"] -> [lemma="SŒUR"]` transforme la propriété `lemma` quand elle vaut `SOEUR` en `SŒUR`. La règle `[form="madame" lemma="MONSIEUR"] -> [lemma="MADAME"]` recherche tous les lemmes dont la forme est `madame` et le lemme `MONSIEUR` et remplace le lemme par `MADAME`. Les autres propriétés ne sont pas modifiées.

### Règle de substitution par regex
La règle `[lemma=/(.*)OE(U.*)/] -> [lemma={$1.'Œ'.$2}]` recherche tous les lemmes dont le lemme contient `OEU`. La partie du lemme qui précède `OEU` est placée dans la variable `$1` (première parenthèse), et la partie qui suit (y compris le `U`) est placée dans la variable `$2` (deuxième parenthèse). La partie droite contient du code _Perl_ entre `{` accolades `}`. Dans le cas présent, le code concatène le contenu de la variable `$1`, de la chaîne `Œ`, et de la chaîne `$2`. Concrètement, cette règle remplace dans tous les lemmes la chaîne `OEU` par `ŒU`.

Autre exemple: la règle `[pos=/([A-Z])([A-Z]+)/] -> [pos={uc($1).lc($2)}]` cherche tous les tokens dont la propriété `pos` contient contient au moins 2 caractères, et dont tous les caractères sont des majuscules. Ces caractères sont reformatés pour toujours commencer par une majuscule et être suivis de minuscules.

### Requêtes multitokens
Les parties gauche et droite d'une règle peuvent comporter plusieurs tokens. Par exemple, la règle `[form=/([Pp]uis)je/] -> [form={$w1_form_1} lemma="POUVOIR" pos="Vvc"] [form="je" lemma="JE" pos="Pp"]` remplace les formes `puisje` par deux tokens; le premier a pour forme `Puis` ou `puis` (forme exacte récupérée dans la première parenthèse de la regex), le second token a pour forme `je`.

Afin de distinguer les variables issues des différents tokens, le nom de ces dernières est de la forme `$wX_Y_Z`, où `X` est le numéro du token (1 pour le 1er token), `Y` est le nom de la proriété (par exemple `form`) et `Z` est le numéro de la parenthèse dans la regex (1 pour la 1ère parenthèse de la regex).

Attention de bien séparer chaque `[` token `]` par un espace: `[form="untoken"] [form="unautretoken"]` est correct, mais pas `[form="untoken"][form="unautretoken"]`.

### Utilisation de propriétés tirées des balises
En mode mixte, les propriétés des balises XML sont accessibles suivant une syntaxe inspirée de XPath, c'est à dire `nomBalise@nomAttribut="unevaleur"`.
Par exemple, la règle `[presto:pretag@pos=/(.*)/] -> [pos={$w1_presto:pretag@pos_1}]` récupère dans `$w1_presto:pretag@pos_1` les valeurs de la propriété `pos` des balises `presto:pretag`, et l'écrit dans la propriété (=colonne) `pos` de chaque token situé dans cette balise.
Ainsi, le code XML suivant:
```xml
form  pos
<presto:pretag pos="Xe">
Lorem
Ipsum
</presto:pretag>
```
sera réécrit:
```xml
form  pos
<presto:pretag pos="Xe">
Lorem Xe
Ipsum Xe
</presto:pretag>
```

Autre exemple: la règle `[presto:text@dateCopyEdition<"1800" lemma=/(.*\|)?HOME(\|.*)?/] -> [lemma={$1.$2}]` va supprimer la chaîne `|HOME|` de tous les tokens présents dans une balise `presto:text` ayant pour attribut `dateCopyEdition` inférieur à 1800.

## Paramètres
 * -d | --debug : mode debug.
 * -h | --header : forcer un en-tête (noms des colonnes séparés par des virgules, par exemple `-h "form,pos,lemma"`).
 * -r <Path> | --rules <Path> : chemin vers le fichier contenant les règles.
 * -q | --quiet : sortie silencieuse (les erreurs ne seront pas signalées).
 * -v | --verbose : sortie bavarde.
 * -x | --xml : activer le mode mixte (prise en charge du XML).

## Exemple d'utilisation

`cat monLexique.csv | perl components/prestoLexicalRules/lexicalRules.pl -r monFichierDeRègles.txt`

## Installation
Ce script en _Perl_ requiert les dépendances suivantes:
 * cpan -i File::Basename
 * cpan -i Getopt::Long
 * cpan -i JSON::XS
 * cpan -i Switch
 * cpan -i Text:CSV_XS
 * cpan -i XML::Entities

## Nota Bene

### Nombre de variables
On peut utiliser jusqu'à 10 parenthèses capturantes par regex (variables `$1` à `$10`).

### Sécurité
Les règles peuvent contenir du code _Perl_. Il convient donc de faire attention lorsqu'on utilise des règles provenant d'une source non fiable; le code contenu dans le fichier de règles pouvant être exécuté.

### Caractères 0
Les contenu des colonnes comportant uniquement un caractère `0` est automatiquement converti en `_0_`.

## Crédits
Développement: Achille Falaise

Financement:
* projet Presto, ANR, laboratoire ICAR
* labex ASLAN, CNRS, laboratoire ICAR

## Licence
Licence GPL v3
